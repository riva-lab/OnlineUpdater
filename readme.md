# OnlineUpdaterPkg

A **FreePascal** package for **Lazarus IDE** that implements several classes for updating your application by using single zip archive from online repository. The package allows you to embed the updater in your application. Thus, no other external executables are required. Your app already contains full functionality of the updater inside and become self-updated. Just manage this process.

Notes:

1. > Updates must be in the public domain. OnlineUpdater uses public data without any authorization. The channel security is provided by HTTPS protocol.

2. > OnlineUpdater does not have a scheduling algorithm. You need to implement your own algorithm for scheduling update check times.

3. > Package was tested only on the Windows 10.

Supported repository servers:

| Service         | Server                  | Class                 |
| --------------- | ----------------------- | --------------------- |
| **GitLab**      | https://gitlab.com      | `TGitlabUpdater`      |
| **GitHub**      | https://api.github.com  | `TGithubUpdater`      |
| **SourceForge** | https://sourceforge.net | `TSourceForgeUpdater` |

## Features

- Built-in functionality in the target application. No external executables.

- Different servers support.

- Single zip archive for update.

- "What's new" data in user defined format.

- Flexible managing of updating sequence.

- Auto-detecting executable version.

- Regular expressions support for filename templates.

## Dependencies

The package depends on [OpenSSL](https://www.openssl.org/), which is used for HTTPS secure communication.

## How to use

1. In **Lazarus IDE** click `Package > Open package file (.lpk)`.
2. Select and open **OnlineUpdaterPkg.lpk**.
3. In package window click `Use > Add to Project`.
4. In your code in `uses` section add `OnlineUpdater`.
5. Use it and enjoy.
6. See usage example in [example](example) directory: example project `ou_test.lpi`.

> For proper work with HTTPS you must have OpenSSL libraries installed. Compile OpenSSL libraries and install it to your system. Or put compiled libraries in your app binary directory. If you don't want compile it yourself use precompiled libraries, e.g. [for Windows](https://kb.firedaemon.com/support/solutions/articles/4000121705). If you include OpenSSL software in your product don't forget about OpenSSL license.

## API

### Requirements

The table summarizes the differences between the updater classes for different servers.

> **Warning!** Do not use instance of `TOnlineUpdater` class! This class is the ancestor class for specific classes for a particular server such as `TGitlabUpdater` or similar.

| Type     | Requirement     | TGitlabUpdater                           | TGithubUpdater                                   | TSourceForgeUpdater                                |
| -------- | --------------- |:----------------------------------------:|:------------------------------------------------:|:--------------------------------------------------:|
| Internal | Server          | [https://gitlab.com](https://gitlab.com) | [https://api.github.com](https://api.github.com) | [https://sourceforge.net](https://sourceforge.net) |
| App repo | Version tag     | :red_circle:                             | :red_circle:                                     |                                                    |
| App repo | Version catalog |                                          |                                                  | :red_circle:                                       |
| Property | `User`          |                                          | :red_circle:                                     |                                                    |
| Property | `Project`       |                                          | :red_circle:                                     | :red_circle:                                       |
| Property | `ProjectID`     | :red_circle:                             |                                                  |                                                    |
| Property | `ReleasesPath`  |                                          |                                                  | :large_blue_circle:                                |

Marks: :red_circle: — required; :large_blue_circle: — optional; *space* — not used at all.

### Common settings

These properties are readable and writable. You can configure it to control the operation of the updater.

| Property           | Type     | Default   | Description                                                                                                                                                                                                                                                                                       |
| ------------------ |:--------:|:---------:| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `CurrentVersion`   | TVersion | `0.0.0.0` | Current actual version of your application. Not nesessary, because updater can read version of this executable itself. If you want version autodetecting, set the checkbox "*Include version info in executable*" in the project settings.                                                        |
| `RootPath`         | String   | `''`      | Path that contains whole your project which must be updated. Path must be relative to executable.                                                                                                                                                                                                 |
| `UpdateDir`        | String   | `''`      | Name of the directory to which the update zip file will be extracted.                                                                                                                                                                                                                             |
| `ZipName`          | String   | `''`      | Name without extension of server-side zip file that contains files for update. Zip archive dirs and files structure must be the same as one of your project. Warning! Do not include extension `.zip`! You can also use regular expression for zip filename. See `UseRegex` property description. |
| `WhatsNewName`     | String   | `''`      | Filename with extension of file with info what's new in update. It's usually a text file. But you can use any format. If `WhatsNewNeed` is `True` and you didn't specify `WhatsNewName` (empty string, i.e. `''`) then `WhatsNewData` will be filled with release description.                    |
| `WhatsNewNeed`     | Boolean  | `False`   | Specifies whether the updater should look for the "*what's new*" file on the server.                                                                                                                                                                                                              |
| `UseRegex`         | Boolean  | `False`   | Specifies whether the updater should use regular expression to search filenames. This affects the `ZipName`, `WhatsNewName` and `ReleasesPath` (`TSourceForgeUpdater` class only) properties. This is useful when your filenames contains modifiable data such as version.                        |
| `RunAfterReplace`  | Boolean  | `True`    | Specifies whether the updater should run new app automatically after successfull old files replacing.                                                                                                                                                                                             |
| `ZipDelete`        | Boolean  | `True`    | Specifies whether the updater should delete zip file after successfull unzipping.                                                                                                                                                                                                                 |
| `OldExeDelete`     | Boolean  | `False`   | Specifies whether the updater should delete old executable (with suffix .old) after the new executable started.                                                                                                                                                                                   |
| `BackupPageLatest` | Boolean  | `False`   | *For debug purposes.* This flag is typically used to develop new updater classes, e.g. for a new server. If `BackupPageLatest` is `True`, the `.txt` file with the server response will be saved to disk.                                                                                         |

### Current state

These properties are read-only and show the current state of the updater.

| Property        | Type           | Description                                                                                |
| --------------- |:--------------:| ------------------------------------------------------------------------------------------ |
| `LatestVer`     | TVersion       | Latest available version of application.                                                   |
| `WhatsNewData`  | String         | Data of "*what's new*" file or release description.                                        |
| `DownloadURL`   | String         | The URL of the zip file for the update.                                                    |
| `DownloadSize`  | Integer        | The size in bytes of the zip file for the update.                                          |
| `IsOutdated`    | Boolean        | Indicates whether the current application is out of date, i.e. `LatestVer` > `CurrentVer`. |
| `IsChecking`    | Boolean        | Value is `True` while the updater is checking for updates.                                 |
| `IsDownloading` | Boolean        | Value is `True` while the updater is downloading the zip file.                             |
| `Status`        | TUpdaterStatus | The updater status.                                                                        |

### Methods

```pascal
procedure StartChecking
```

> Method starts checking for updates.
> 
> You can verify if the check has started by getting the value of the `IsChecking` property.

---

```pascal
procedure StartDownloading(AForce: Boolean = False)
```

> Method starts downloading zip file and unzips it after successfull download. Must be called after successfull finish of the `StartChecking` method. Otherwise no action is performed.
> 
> You can verify if the download has started by getting the value of the `IsDownloading` property.

---

```pascal
procedure StartFullUpdating
```

> Method combines 2 previously described actions: `StartChecking` method is performed, then `StartDownload` method is performed.

---

```pascal
function StartReplacing: Boolean
```

> Method starts replacing old files to perform the upgrade.
> 
> Returns `True` if the replacing was actually started.
> 
> Usually called at application startup, for example in the `FormShow` handler.

---

```pascal
function IsReplacing: Boolean
```

> Method begins updating process.
> 
> If the zip file of the new application was succsessfully downloaded and unzipped then this method copies current running executable into a new file with `old` suffix and runs it. If these steps were completed without errors, the method returns `True`, and your application should be terminated as soon as possible so that the application with `old` suffix can perform files replacement.

---

### Working flow and events

| If failed | Stage, top to bottom           | If done                                 |
|:---------:|:------------------------------:|:---------------------------------------:|
|           | Call `StartChecking;`          |                                         |
| `OnError` | Checking for updates           | `OnAvailable` (if newer version exists) |
| `OnError` | Call `StartDownloading;`       | `OnDownloadBegin`                       |
|           | Downloading zip file           | `OnDownloading` (continuously)          |
| `OnError` | Downloading completed          | `OnDownloadDone`                        |
| `OnError` | Start uzipping downloaded file |                                         |
|           | Unzipping downloaded file      | `OnUnzipping` (continuously)            |
| `OnError` | Unzipping completed            | `OnUnzip`                               |
|           | Call `IsReplacing;`            |                                         |
|           | Application closed             |                                         |
|           | "Old" application started      |                                         |
|           | Call `StartReplacing;`         | `OnReplaceBegin`                        |
|           | Old files replacing            |                                         |
| `OnError` | Replacing completed            | `OnReplaceDone`                         |
|           | "Old" application closed       |                                         |
|           | Updated app started            |                                         |

## License

Package releases under [MIT License](license.txt). Copyright (c) 2023 Riva.
