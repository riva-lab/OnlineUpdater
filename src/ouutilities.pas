{ ouUtilities.pas                                  |  (c) 2023 Riva   |  v1.0  |
  ------------------------------------------------------------------------------
  This unit is a part of OnlineUpdater package.
  Several utility functions and procedures.
  ------------------------------------------------------------------------------
  (c) Riva, 2023.10.27
  https://riva-lab.gitlab.io        https://gitlab.com/riva-lab

  Functions EncodeUrl and DecodeUrl are provided by BenJones:
  https://wiki.freepascal.org/URL_encoding/decoding
  ==============================================================================

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  -----------------------------------------------------------------------------}

unit ouUtilities;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, process;

function ExecuteProgram(AFileName: String; AParameters: array of String): Boolean;
function ExecuteProgram(AFileName: String): Boolean;

function GetOldExecutableName: String;
function IsOldExecutable: Boolean;

function EncodeUrl(const url: String): String;
function DecodeUrl(const url: String): String;


implementation

function ExecuteProgram(AFileName: String; AParameters: array of String): Boolean;
  var
    p: TProcess;
    i: Integer;
  begin
    p := TProcess.Create(nil);

      try
      p.InheritHandles := False;
      p.Options        := [];
      p.ShowWindow     := swoShow;

      for i := 1 to GetEnvironmentVariableCount do
        p.Environment.Add(GetEnvironmentString(i));

      if Length(AParameters) > 0 then
        for i := 0 to High(AParameters) do
          p.Parameters.Add(AParameters[i]);

      p.Executable := AFileName;
      p.Execute;
      finally
      Result       := p.Active;
      p.Free;
      end;
  end;

function ExecuteProgram(AFileName: String): Boolean;
  begin
    Result := ExecuteProgram(AFileName, []);
  end;

function GetOldExecutableName: String;
  begin
    Result := Format('%s%s.old%s', [ExtractFilePath(ParamStr(0)),
      ExtractFileNameOnly(ParamStr(0)), ExtractFileExt(ParamStr(0))]);
  end;

function IsOldExecutable: Boolean;
  begin
    Result := ExtractFileExt(ExtractFileNameOnly(ParamStr(0))) = '.old';
  end;

function EncodeUrl(const url: String): String;
  var
    x:     Integer;
    sBuff: String;
  const
    SafeMask = ['A'..'Z', '0'..'9', 'a'..'z', '*', '@', '.', '_', '-'];
  begin
    //Init
    sBuff := '';

    for x := 1 to Length(url) do
      begin
        //Check if we have a safe char
      if url[x] in SafeMask then
        begin
        //Append all other chars
        sBuff := sBuff + url[x];
        end
      else if url[x] = ' ' then
        begin
        //Append space
        sBuff := sBuff + '+';
        end
      else
        begin
        //Convert to hex
        sBuff := sBuff + '%' + IntToHex(Ord(url[x]), 2);
        end;
      end;

    Result := sBuff;
  end;

function DecodeUrl(const url: String): String;
  var
    x:    Integer;
    ch:   String;
    sVal: String;
    Buff: String;
  begin
    //Init
    Buff := '';
    x    := 1;
    while x <= Length(url) do
      begin
      //Get single char
      ch := url[x];

      if ch = '+' then
        begin
        //Append space
        Buff := Buff + ' ';
        end
      else if ch <> '%' then
        begin
        //Append other chars
        Buff := Buff + ch;
        end
      else
        begin
        //Get value
        sVal := Copy(url, x + 1, 2);
        //Convert sval to int then to char
        Buff := Buff + char(StrToInt('$' + sVal));
        //Inc counter by 2
        Inc(x, 2);
        end;
      //Inc counter
      Inc(x);
      end;
    //Return result
    Result := Buff;
  end;

end.
