{ ouVersion.pas                                    |  (c) 2023 Riva   |  v1.0  |
  ------------------------------------------------------------------------------
  This unit is a part of OnlineUpdater package.
  Reading version info from running executable. Based on info from
  https://wiki.freepascal.org/Show_Application_Title,_Version,_and_Company
  ------------------------------------------------------------------------------
  (c) Riva, 2023.10.29
  https://riva-lab.gitlab.io        https://gitlab.com/riva-lab
  ==============================================================================

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  -----------------------------------------------------------------------------}

unit ouVersion;

{$mode objfpc}{$H+}

interface

type

  { TFileVersionInfoSimple }

  TFileVersionInfoSimple = class

  private
    FCompanyName:      String;
    FFileDescription:  String;
    FFileVersion:      String;
    FInternalName:     String;
    FLegalCopyright:   String;
    FOriginalFilename: String;
    FProductName:      String;
    FProductVersion:   String;
    FComments:         String;

  public
    property CompanyName: String read FCompanyName;
    property FileDescription: String read FFileDescription;
    property FileVersion: String read FFileVersion;
    property InternalName: String read FInternalName;
    property LegalCopyright: String read FLegalCopyright;
    property OriginalFilename: String read FOriginalFilename;
    property ProductName: String read FProductName;
    property ProductVersion: String read FProductVersion;
    property Comments: String read FComments;

    function ReadVersionInfo: TFileVersionInfoSimple;
  end;


implementation

uses
  FileInfo;


{ TFileVersionInfoSimple }

function TFileVersionInfoSimple.ReadVersionInfo: TFileVersionInfoSimple;
  begin
    with TFileVersionInfo.Create(nil) do
        try
          try
          FileName := ParamStr(0);
          ReadFileInfo;

          FCompanyName      := VersionStrings.Values['CompanyName'];
          FFileDescription  := VersionStrings.Values['FileDescription'];
          FFileVersion      := VersionStrings.Values['FileVersion'];
          FInternalName     := VersionStrings.Values['InternalName'];
          FLegalCopyright   := VersionStrings.Values['LegalCopyright'];
          FOriginalFilename := VersionStrings.Values['OriginalFilename'];
          FProductName      := VersionStrings.Values['ProductName'];
          FProductVersion   := VersionStrings.Values['ProductVersion'];
          FComments         := VersionStrings.Values['Comments'];

          finally
          Free;
          end;
        Result := Self;
        except
        Result := nil;
        end;
  end;

end.
