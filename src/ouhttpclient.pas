{ ouHttpClient.pas                                 |  (c) 2023 Riva   |  v1.0  |
  ------------------------------------------------------------------------------
  This unit is a part of OnlineUpdater package.
  Simplified friendly functions to use HTTP client.
  ------------------------------------------------------------------------------
  (c) Riva, 2023.10.27
  https://riva-lab.gitlab.io        https://gitlab.com/riva-lab
  ==============================================================================

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  -----------------------------------------------------------------------------}

unit ouHttpClient;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, fphttpclient, opensslsockets;

type
  THttpsDataEvent = TDataEvent;

const
  CUserAgent = 'AppRequest/1.0';

function HTTPClientGet(AURL: String; AStream: TStream): Boolean;
function HTTPClientGet(AURL: String; AStrings: TStrings): Boolean;
function HTTPClientGet(AURL: String): String;
function HTTPClientGetFile(AURL: String; AFilename: String; AOnGetting: THttpsDataEvent = nil): Boolean;
function HTTPClientGetContentSize(AURL: String): Integer;


implementation

function HTTPClientGet(AURL: String; AStream: TStream; AStrings: TStrings;
  AOnGetting: THttpsDataEvent): Boolean;
  begin
      try
      with TFPHTTPClient.Create(nil) do
          try
          AllowRedirect  := True;
          KeepConnection := False;
          OnDataReceived := AOnGetting;
          AddHeader('User-Agent', CUserAgent);
          if Assigned(AStream) then
            Get(AURL, AStream)
          else
            Get(AURL, AStrings);
          finally
          Free;
          end;
      Result := True;
      except
      Result := False;
      end;
  end;

function HTTPClientGet(AURL: String; AStream: TStream): Boolean;
  begin
    Result := HTTPClientGet(AURL, AStream, nil, nil);
  end;

function HTTPClientGet(AURL: String; AStrings: TStrings): Boolean;
  begin
    Result := HTTPClientGet(AURL, nil, AStrings, nil);
  end;

function HTTPClientGet(AURL: String): String;
  var
    strList: TStringList;
  begin
    strList := TStringList.Create;
    HTTPClientGet(AURL, strList);
    Result  := strList.Text;
    strList.Free;
  end;

function HTTPClientGetFile(AURL: String; AFilename: String;
  AOnGetting: THttpsDataEvent): Boolean;
  var
    newFile: TFileStream;
  begin
      try
      newFile := TFileStream.Create(AFilename, fmCreate);
      Result  := HTTPClientGet(AURL, newFile, nil, AOnGetting);
      finally
      newFile.Free;
      end;
  end;

function HTTPClientGetContentSize(AURL: String): Integer;
  var
    response: TStringList;
  begin
      try
      response := TStringList.Create;
      TFPHTTPClient.Head(AURL, response);
      Result   := StrToIntDef(response.Values['Content-Length'], 0);
      except
      Result   := 0;
      end;
    response.Free;
  end;

end.
