{ OnlineUpdater.pas                                |  (c) 2023 Riva   |  v1.1  |
  ------------------------------------------------------------------------------
    A FreePascal package for Lazarus IDE that implements several classes
  for updating your application by using zip archive from online repository.
    The package allows you to embed the updater in your application.
  Thus, no other external executables are required. Your app already
  contains full functionality of the updater inside and become self-updated.
  Just manage this process.
    Notes:
      1. Updates must be in the public domain. OnlineUpdater uses public data
         without any authorization. The channel security is provided
         by HTTPS protocol.
      2. OnlineUpdater does not have a scheduling algorithm. You need
         to implement your own algorithm for scheduling update check times.
    Currently supported servers:
      - gitlab.com      - TGitlabUpdater class,
      - github.com      - TGithubUpdater class,
      - sourceforge.net - TSourceForgeUpdater class.
  ------------------------------------------------------------------------------
  (c) Riva, 2023.10.25
  https://riva-lab.gitlab.io        https://gitlab.com/riva-lab
  ==============================================================================

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  ==============================================================================

  Versions:
  ------------------------------------------------------------------------------
  v1.1    2023.10.30   added TSourceForgeUpdater class
  -----------------------------------------------------------------------------}

unit OnlineUpdater;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, FileUtil, FileInfo,
  ouHttpClient, ouUtilities, ouVersion, Zipper, regexpr, fpjson, XMLRead, DOM;

type

  TVersion = TProgramVersion;

  TUpdaterStatus = (usOK, usWorking, usDownloading, usUnchecked, usOffline, usNoUpdates,
    usNoFile, usInvalidLink, usDLError, usZipError, usNotReplaced, usError);

  { TOnlineUpdater
    --------------
    Reference class for updating your application from online repository.
    Allows you to implement updating with a single ZIP-archive.
    ZIP must be placed into release assets and must have .zip extension!
    ZIP must have the same files and dirs structure as your app one in root dir.
    Note that this class is the basis.
    You must implement custom class for your specific online server.
    See TGitlabUpdater class for example.
    --------
    (с) Riva, 2023.10.25
    https://riva-lab.gitlab.io
    https://gitlab.com/riva-lab }

  TOnlineUpdater = class(TThread)

  const
    CUpdateDirDef = '_OnUpLastRelease'; // default dir name for new files
    CGetAttempts  = 3;                  // max attempts to get response from server
    CGetInterval  = 2000;               // [ms] delay before resend request

  private
    FCurrentVer:      TVersion;
    FLatestVer:       TVersion;
    FProjectID:       String;
    FUser:            String;
    FProject:         String;
    FReleasesPath:    String;
    FZipName:         String;
    FZipFileName:     String;
    FUpdateDir:       String;
    FRootPath:        String;
    FDownloadURL:     String;
    FWhatsNewURL:     String;
    FWhatsNewData:    String;
    FWhatsNewName:    String;
    FIsOutdated:      Boolean;
    FIsChecking:      Boolean;
    FIsDownloading:   Boolean;
    FUseRegex:        Boolean;
    FZipDelete:       Boolean;
    FReplaceStart:    Boolean;
    FRunAfterRplc:    Boolean;
    FOldExeDelete:    Boolean;
    FWhatsNewNeed:    Boolean;
    FBUPageLatest:    Boolean;
    FDownloadSize:    Integer;
    FStatus:          TUpdaterStatus;
    FOnAvailable:     TThreadMethod;
    FOnDownloadBegin: TThreadMethod;
    FOnDownloading:   THttpsDataEvent;
    FOnDownloadDone:  TThreadMethod;
    FOnUnzipping:     TProgressEventEx;
    FOnUnzip:         TThreadMethod;
    FOnReplaceBegin:  TThreadMethod;
    FOnReplaceDone:   TThreadMethod;
    FOnError:         TThreadMethod;

    function IsReadyToReplace: Boolean;
    function IsServerOnline: Boolean;
    function GetLatestReleaseURL: String; virtual; abstract;
    function GetLatestReleasePage: Boolean; virtual;
    function ExtractLatestVersion: Boolean; virtual; abstract;
    function CheckUpdatingNecessity: Boolean;
    function GetWhatsNewData: Boolean;
    function ExtractMetaData: Boolean; virtual; abstract;
    function DownloadLatestRelease: Boolean;
    function UnzipLatestRelease: Boolean;
    function ReplaceOldFiles: Boolean;

    procedure Execute; override;
    procedure TaskReplacing;
    procedure TaskOldExeDeleting;
    procedure TaskChecking;
    procedure TaskDownloading;
    procedure DoEvent(AMethod: TThreadMethod);

  protected
    FServerRootURL: String;
    FPageLatestURL: String;
    FPageLatest:    TStringStream;

    function IsDownloadURLsGiven(const AURL: String; const ASize: String = ''): Boolean;
    function IsWhatsNewURLsGiven(const AURL: String): Boolean;

    property User: String read FUser write FUser;
    property Project: String read FProject write FProject;
    property ProjectID: String read FProjectID write FProjectID;
    property ReleasesPath: String read FReleasesPath write FReleasesPath;

  public
    constructor Create;
    destructor Destroy; override;


    // UPDATER'S CONTROL METHODS:

    { Method starts checking for updates. You can verify if the check has started
      by getting the value of the `IsChecking` property. }
    procedure StartChecking;

    { Method starts downloading zip file and unzips it after successfull download.
      Must be called after successfull finish of the `StartChecking` method.
      Otherwise no action is performed. You can verify if the download has started
      by getting the value of the `IsDownloading` property. }
    procedure StartDownloading(AForce: Boolean = False);

    { Method combines 2 previously described actions: `StartChecking` method
      is performed, then `StartDownload` method is performed. }
    procedure StartFullUpdating;

    { Method starts replacing old files to perform the upgrade. Returns `True`
      if the replacing was actually started. Usually called at application startup,
      for example in the `FormShow` handler. }
    function StartReplacing: Boolean;

    { Method begins updating process. If the zip file of the new application was
      succsessfully downloaded and unzipped then this method copies current running
      executable into a new file with `old` suffix and runs it. If these steps were
      completed without errors, the method returns `True`, and your application
      should be terminated as soon as possible so that the application with
      `old` suffix can perform files replacement. }
    function IsReplacing: Boolean;


    // UPDATER'S CURRENT STATE:

    { Latest available version of application. }
    property LatestVer: TVersion read FLatestVer;

    { Data of "what's new" file or release description. }
    property WhatsNewData: String read FWhatsNewData;

    { The URL of the zip file for the update. }
    property DownloadURL: String read FDownloadURL;

    { The size in bytes of the zip file for the update. }
    property DownloadSize: Integer read FDownloadSize;

    { Indicates whether the current application is out of date,
      i.e. `LatestVer` > `CurrentVer`. }
    property IsOutdated: Boolean read FIsOutdated;

    { Value is `True` while the updater is checking for updates. }
    property IsChecking: Boolean read FIsChecking;

    { Value is `True` while the updater is downloading the zip file. }
    property IsDownloading: Boolean read FIsDownloading;

    { The updater status. }
    property Status: TUpdaterStatus read FStatus;


    // COMMON SETTINGS FOR UPDATER:

    { Current actual version of your application. Not nesessary, because
      updater can read version of this executable itself. If you want version
      autodetecting, set the checkbox "Include version info in executable"
      in the project settings. }
    property CurrentVersion: TVersion read FCurrentVer write FCurrentVer;

    { Path that contains whole your project which must be updated. 
      Path must be relative to executable. }
    property RootPath: String read FRootPath write FRootPath;

    { Name of the directory to which the update zip file will be extracted. }
    property UpdateDir: String read FUpdateDir write FUpdateDir;

    { Filename with extension of file with info what's new in update. 
      It's usually a text file. But you can use any format. If `WhatsNewNeed`
      is `True` and you didn't specify `WhatsNewName` (empty string, i.e. `''`) 
      then `WhatsNewData` will be filled with release description. }
    property WhatsNewName: String read FWhatsNewName write FWhatsNewName;

    { Specifies whether the updater should look for
      the "what's new" file on the server. }
    property WhatsNewNeed: Boolean read FWhatsNewNeed write FWhatsNewNeed;

    { Name without extension of server-side zip file that contains files
      for update. Zip archive dirs and files structure must be the same
      as one of your project. Warning! Do not include extension `.zip`! 
      You can also use regular expression for zip filename.
      See `UseRegex` property description. }
    property ZipName: String read FZipName write FZipName;

    { Specifies whether the updater should use regular expression to search
      filenames. This affects the `ZipName`, `WhatsNewName` and `ReleasesPath`
      (`TSourceForgeUpdater` class only) properties. This is useful when your
      filenames contains modifiable data such as version. }
    property UseRegex: Boolean read FUseRegex write FUseRegex;

    { Specifies whether the updater should run new app automatically after 
      successfull old files replacing. }
    property RunAfterReplace: Boolean read FRunAfterRplc write FRunAfterRplc;

    { Specifies whether the updater should delete zip file
      after successfull unzipping. }
    property ZipDelete: Boolean read FZipDelete write FZipDelete;

    { Specifies whether the updater should delete old executable
      (with suffix .old) after the new executable started. }
    property OldExeDelete: Boolean read FOldExeDelete write FOldExeDelete;


    // FOR DEVELOPERS:

    { For debug purposes. This flag is typically used to develop new updater
      classes, e.g. for a new server. If `BackupPageLatest` is `True`, 
      the `.txt` file with the server response will be saved to disk. }
    property BackupPageLatest: Boolean read FBUPageLatest write FBUPageLatest;


    // UPDATER'S EVENTS:

    property OnAvailable: TThreadMethod read FOnAvailable write FOnAvailable;
    property OnDownloadBegin: TThreadMethod read FOnDownloadBegin write FOnDownloadBegin;
    property OnDownloading: THttpsDataEvent read FOnDownloading write FOnDownloading;
    property OnDownloadDone: TThreadMethod read FOnDownloadDone write FOnDownloadDone;
    property OnUnzipping: TProgressEventEx read FOnUnzipping write FOnUnzipping;
    property OnUnzip: TThreadMethod read FOnUnzip write FOnUnzip;
    property OnReplaceBegin: TThreadMethod read FOnReplaceBegin write FOnReplaceBegin;
    property OnReplaceDone: TThreadMethod read FOnReplaceDone write FOnReplaceDone;
    property OnError: TThreadMethod read FOnError write FOnError;
  end;


  { TGitlabUpdater
    --------------
    Implementation of updater for Gitlab.com server.
    You must specify 1 mandatory property:
    - ProjectID - your project ID (looks like a number),
      you can find it on the project homepage next to the project name.
    Please use tags in 'versioned style' to name your releases.
    Tag name should look like '1.2' or 'v1.2.3' etc.
    The tag name is used to identify the latest version. }

  TGitlabUpdater = class(TOnlineUpdater)

  private
    function GetLatestReleaseURL: String; override;
    function ExtractLatestVersion: Boolean; override;
    function ExtractMetaData: Boolean; override;

  public
    constructor Create(AProjectID: String = '');

    // ID of your project (it looks like a number)
    property ProjectID;
  end;


  { TGithubUpdater
    --------------
    Implementation of updater for Github.com server.
    You must specify 2 mandatory properties:
    - User - author's username (repository owner),
    - Project - project name,
    e.g. https://github.com/<User>/<Project>/
    Please use tags in 'versioned style' to name your releases.
    Tag name should look like '1.2' or 'v1.2.3' etc.
    The tag name is used to identify the latest version. }

  TGithubUpdater = class(TOnlineUpdater)

  private
    function GetLatestReleaseURL: String; override;
    function ExtractLatestVersion: Boolean; override;
    function ExtractMetaData: Boolean; override;

  public
    constructor Create(AUser: String = ''; AProject: String = '');

    // author's username (owner of repository)
    property User;

    // name of the project
    property Project;
  end;


  { TSourceForgeUpdater
    -------------------
    Implementation of updater for SourceForge.net server.
    You must specify 2 properties:
    - Project - name of your project,
      e.g. https://sourceforge.net/projects/<Project>/files/<...>
    - ReleasesPath - [optional] path to directory that contains releases subdirs:
      e.g. Home / <ReleasesPath> / 1.2.3
    Note that release subdirs should be named in 'version style',
    e.g. '1.2' or 'v1.2.3' etc. This name is used to identify the latest version.
    Please try to avoid using subdirs in release subdir.
    But this isn't a requirement. }

  TSourceForgeUpdater = class(TOnlineUpdater)

  private
    FLatestVerStr: String;

    function GetLatestReleaseURL: String; override;
    function ExtractLatestVersion: Boolean; override;
    function ExtractMetaData: Boolean; override;

  public
    constructor Create(AProject: String = '');

    { Name of your project,
      e.g. https://sourceforge.net/projects/<Project>/files/<...> }
    property Project;

    { Path to catalog that contains releases subcatalogs:
      e.g. Home / <ReleasesPath> / 1.2.3}
    property ReleasesPath;
  end;


  { Utility functions }

function ParseVersion(AString: String): TVersion;
function Version(AMajor, AMinor, ARev, ABuild: Word): TVersion;


implementation

type

  TBoolFunc = function: Boolean of object;


function ParseVersion(AString: String): TVersion;
  begin
    with TRegExpr.Create('\w*?(\d+)(?:[\s-_/;:,\.]+(\d+))?'
        + '(?:[\s-_/;:,]*(?:|r|r|rev)(?:\.|:|-|=)?\s*(\d+))?'
        + '(?:[\s-_/;:,]*(?:|b|bld|build|beta|alpha)(?:\.|:|-|=)?\s*(\d+))?') do
        try
          try
          InputString := AString;
          ModifierI   := True;
          if Exec then
            Result := Version(
              StrToIntDef(String(Match[1]), 0),
              StrToIntDef(String(Match[2]), 0),
              StrToIntDef(String(Match[3]), 0),
              StrToIntDef(String(Match[4]), 0));
          except
          Result := Version(0, 0, 0, 0);
          end;
        finally
        Free;
        end;
  end;

function Version(AMajor, AMinor, ARev, ABuild: Word): TVersion;
  begin
    Result.Major    := AMajor;
    Result.Minor    := AMinor;
    Result.Revision := ARev;
    Result.Build    := ABuild;
  end;

function TryDoBoolFunc(ABoolFunc: TBoolFunc; AAttempts, AInterval: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;

    if (AAttempts > 0) and Assigned(ABoolFunc) then
      for i := 0 to AAttempts - 1 do
        begin
        Result := ABoolFunc();
        if Result then Break;
        Sleep(AInterval);
        end;
  end;

function TryDoBoolFunc(ABoolFunc: TBoolFunc): Boolean;
  begin
    Result := TryDoBoolFunc(ABoolFunc,
      TOnlineUpdater.CGetAttempts, TOnlineUpdater.CGetInterval);
  end;


{ TOnlineUpdater }

procedure TOnlineUpdater.Execute;
  begin
    while not Terminated do
      begin
      FStatus := usWorking;

      if FReplaceStart then TaskReplacing
      else
      if FOldExeDelete then TaskOldExeDeleting
      else
        begin
        if FUpdateDir = '' then FUpdateDir := CUpdateDirDef;
        FPageLatestURL := GetLatestReleaseURL;
        FZipFileName   := Format('%s%s.zip', [FRootPath, FUpdateDir]);

        if FIsChecking then TaskChecking;
        if not FIsChecking and FIsDownloading then TaskDownloading;

        FIsChecking    := False;
        FIsDownloading := False;
        end;

      if FStatus = usWorking then FStatus := usOK;
      if FStatus <> usOK then DoEvent(FOnError);

      Suspended := True;
      end;
  end;

procedure TOnlineUpdater.TaskReplacing;
  begin
    FReplaceStart := False;
    DoEvent(FOnReplaceBegin);

    if ReplaceOldFiles then
      begin
      FStatus := usOK;
      DoEvent(FOnReplaceDone);
      end
    else
      FStatus := usNotReplaced;
  end;

procedure TOnlineUpdater.TaskOldExeDeleting;
  var
    i: Integer;
  begin
    for i := 0 to 300 do
      if FileExistsUTF8(GetOldExecutableName) then
        begin
        FStatus := usOK;
        if DeleteFile(GetOldExecutableName) then Exit;
        Sleep(100);
        end;

    if FStatus = usOK then
      FStatus := usError;
  end;

procedure TOnlineUpdater.TaskChecking;
  begin
    if TryDoBoolFunc(@IsServerOnline) then
      if TryDoBoolFunc(@GetLatestReleasePage) then
        if ExtractLatestVersion then
          if ExtractMetaData then
            begin
            FStatus := usOK;
            if CheckUpdatingNecessity then
              begin
              GetWhatsNewData;
              FIsChecking := False;
              DoEvent(FOnAvailable);
              end;
            end;
  end;

procedure TOnlineUpdater.TaskDownloading;
  begin
    DoEvent(FOnDownloadBegin);
    if TryDoBoolFunc(@DownloadLatestRelease) then
      begin
      FIsDownloading := False;
      DoEvent(FOnDownloadDone);

      if UnzipLatestRelease then
        begin
        FStatus := usOK;
        DoEvent(FOnUnzip);
        end;
      end;
  end;

procedure TOnlineUpdater.DoEvent(AMethod: TThreadMethod);
  begin
    if Assigned(AMethod) then Synchronize(AMethod);
  end;

function TOnlineUpdater.IsDownloadURLsGiven(const AURL: String; const ASize: String): Boolean;
  var
    fileName: String;
  begin
    Result   := False;
    fileName := LowerCase(DecodeUrl(ExtractFileName(AURL)));
    FZipName := LowerCase(FZipName);

    if AURL = '' then Exit(False);

    if FUseRegex then
      Result := ExecRegExpr(FZipName + '\.zip', fileName)
    else
      Result := fileName = FZipName + '.zip';

    if Result then
      begin
      FDownloadURL := AURL;

      if ASize = '' then
        FDownloadSize := HTTPClientGetContentSize(AURL)
      else
        FDownloadSize := StrToInt64Def(ASize, 0);
      end;
  end;

function TOnlineUpdater.IsWhatsNewURLsGiven(const AURL: String): Boolean;
  var
    fileName: String;
  begin
    Result        := False;
    fileName      := LowerCase(DecodeUrl(ExtractFileName(AURL)));
    FWhatsNewName := LowerCase(FWhatsNewName);

    if AURL = '' then Exit(False);

    if FWhatsNewNeed and (FWhatsNewName <> '') then
      begin
      if FUseRegex then
        Result := ExecRegExpr(FWhatsNewName, fileName)
      else
        Result := FWhatsNewName = fileName;

      if Result then FWhatsNewURL := AURL;
      end;
  end;

function TOnlineUpdater.IsReadyToReplace: Boolean;
  begin
    if FUpdateDir = '' then FUpdateDir := CUpdateDirDef;
    Result := DirectoryExistsUTF8(FRootPath + FUpdateDir);
  end;

function TOnlineUpdater.IsServerOnline: Boolean;
  var
    response: TStringList;
  begin
    Result := False;

      try
      response := TStringList.Create;
      Result   := HTTPClientGet(FServerRootURL, response);
      finally
      response.Free;
      end;

    if not Result then
      FStatus := usOffline;
  end;

function TOnlineUpdater.GetLatestReleasePage: Boolean;
  begin
    FPageLatest.Clear;
    Result := HTTPClientGet(FPageLatestURL, FPageLatest);

    if FBUPageLatest then
      FPageLatest.SaveToFile(Format('%s%s.txt', [FRootPath, FUpdateDir]));

    if not Result then
      FStatus := usInvalidLink;
  end;

function TOnlineUpdater.CheckUpdatingNecessity: Boolean;
  begin
    FIsOutdated    := NewerVersion(FLatestVer, FCurrentVer);
    Result         := FIsOutdated;
    FIsDownloading := FIsDownloading and FIsOutdated;
    if not FIsOutdated then FStatus := usNoUpdates;
  end;

function TOnlineUpdater.GetWhatsNewData: Boolean;
  begin
    Result := False;

    if FWhatsNewNeed then
      if FWhatsNewName <> '' then
        if FWhatsNewURL <> '' then
          FWhatsNewData := HTTPClientGet(FWhatsNewURL);

    Result := FWhatsNewData <> '';
  end;

function TOnlineUpdater.DownloadLatestRelease: Boolean;
  begin
    FStatus := usDownloading;
    Result  := HTTPClientGetFile(FDownloadURL, FZipFileName, FOnDownloading);
    FStatus := usWorking;

    if not Result then
      begin
      FStatus := usDLError;
      DeleteFile(FZipFileName);
      end;
  end;

function TOnlineUpdater.UnzipLatestRelease: Boolean;
  begin
    Result := False;
    if not FileExistsUTF8(FZipFileName) then Exit;

    with TUnZipper.Create do
        try
        FileName     := FZipFileName;
        OutputPath   := FRootPath + FUpdateDir;
        OnProgressEx := FOnUnzipping;
          try
          Examine;
          UnZipAllFiles;
          if FZipDelete then DeleteFile(FZipFileName);
          Result  := True;
          except
          FStatus := usZipError;
          end;
        finally
        Free;
        end;
  end;

function TOnlineUpdater.ReplaceOldFiles: Boolean;
  var
    exeName: String;
    i:       Integer;
  begin
    exeName := ExtractFileNameOnly(ExtractFileNameOnly(ParamStr(0)))
      + ExtractFileExt(ParamStr(0));

    // We must ensure that previous executable was unlocked,
    // so we wait for its successfull deleting
    for i := 0 to 150 do
      if DeleteFile(exeName) then
        break
      else
        Sleep(200);

    // Copying all new files with replacing previous
    Result := CopyDirTree(FRootPath + FUpdateDir, FRootPath,
      [cffCreateDestDirectory, cffOverwriteFile]);

    if not Result then Exit;

    // at this point we have updated files
    // and we are allowed to delete update directory
    DeleteDirectory(FRootPath + FUpdateDir, False);

    // if new executable is missing, restore previous
    if not FileExistsUTF8(exeName) then
      CopyFile(ParamStr(0), ExtractFilePath(ParamStr(0)) + exeName);

    // after all we run new executable
    if FRunAfterRplc and FileExistsUTF8(exeName) then ExecuteProgram(exeName);
  end;

constructor TOnlineUpdater.Create;
  begin
    inherited Create(True);

    FreeOnTerminate  := True;
    FCurrentVer      := Version(0, 0, 0, 0);
    FLatestVer       := FCurrentVer;
    FUser            := '';
    FProject         := '';
    FReleasesPath    := '';
    FProjectID       := '';
    FZipName         := '';
    FZipFileName     := '';
    FUpdateDir       := '';
    FRootPath        := '';
    FDownloadURL     := '';
    FWhatsNewURL     := '';
    FWhatsNewName    := '';
    FWhatsNewData    := '';
    FIsOutdated      := False;
    FIsChecking      := False;
    FIsDownloading   := False;
    FUseRegex        := False;
    FZipDelete       := True;
    FReplaceStart    := False;
    FRunAfterRplc    := True;
    FOldExeDelete    := False;
    FWhatsNewNeed    := False;
    FBUPageLatest    := False;
    FDownloadSize    := 0;
    FStatus          := usUnchecked;
    FOnAvailable     := nil;
    FOnDownloadBegin := nil;
    FOnDownloading   := nil;
    FOnDownloadDone  := nil;
    FOnUnzipping     := nil;
    FOnUnzip         := nil;
    FOnReplaceBegin  := nil;
    FOnReplaceDone   := nil;
    FOnError         := nil;
    FPageLatestURL   := '';
    FServerRootURL   := '';
    FPageLatest      := TStringStream.Create;

    // try to detect version of this executable
    with TFileVersionInfoSimple.Create do
      begin
      if Assigned(ReadVersionInfo) then
        FCurrentVer := ParseVersion(FileVersion);
      Free;
      end;
  end;

destructor TOnlineUpdater.Destroy;
  begin
    FPageLatest.Free;
    inherited Destroy;
  end;

procedure TOnlineUpdater.StartChecking;
  begin
    FOldExeDelete := False;
    FIsChecking   := True;
    Start;
  end;

procedure TOnlineUpdater.StartDownloading(AForce: Boolean);
  begin
    if FStatus = usOK then
      begin
      FIsDownloading := FIsOutdated or AForce;
      if FIsDownloading then FOldExeDelete := False;
      Start;
      end
    else
      DoEvent(FOnError);
  end;

procedure TOnlineUpdater.StartFullUpdating;
  begin
    FOldExeDelete  := False;
    FIsChecking    := True;
    FIsDownloading := True;
    Start;
  end;

function TOnlineUpdater.StartReplacing: Boolean;
  begin
    // if this app is old (has .old suffix) - start copying new files
    Result        := IsReadyToReplace;
    FReplaceStart := Result and IsOldExecutable;
    Start;
  end;

function TOnlineUpdater.IsReplacing: Boolean;
  begin
    // If we have new files to update then we run
    // new app's instance that will update all files.
    Result := IsReadyToReplace and not IsOldExecutable;

    // And for successfull updating we must unlock this app binary file.
    // So we create new executable with .old prefix, then run it.
    if Result then
      if CopyFile(ParamStr(0), GetOldExecutableName) then
        Result := ExecuteProgram(GetOldExecutableName);
  end;


{ TGitlabUpdater }

function TGitlabUpdater.GetLatestReleaseURL: String;
  begin
    Result := Format('%s/api/v4/projects/%s/releases/', [FServerRootURL, FProjectID]);
  end;

function TGitlabUpdater.ExtractLatestVersion: Boolean;
  var
    json: TJSONData;
  begin
    Result := False;

      try
      json       := GetJSON(FPageLatest.DataString);
      FLatestVer := ParseVersion(json.GetPath('[0]').GetPath('tag_name').AsString);
      Result     := True;
      except
      FStatus    := usError;
      end;

    json.Free;
  end;

function TGitlabUpdater.ExtractMetaData: Boolean;
  var
    json, lnka, lnk: TJSONData;
    i:               Integer;
    url:             String;
  begin
    Result := False;

      try
      json := GetJSON(FPageLatest.DataString).GetPath('[0]');
      lnka := json.GetPath('assets').GetPath('links');

      if lnka.Count > 0 then
        for i := 0 to lnka.Count - 1 do
          begin
          lnk    := lnka.GetPath('[' + i.ToString + ']');
          url    := lnk.GetPath('direct_asset_url').AsString;
          Result := Result or IsDownloadURLsGiven(url);
          IsWhatsNewURLsGiven(url);
          end;

      if not Result then FStatus := usNoFile;

      if Result and FWhatsNewNeed and (FWhatsNewName = '') then
        FWhatsNewData := json.GetPath('description').AsString;

      except
      FDownloadSize := 0;
      FDownloadURL  := '';
      FWhatsNewURL  := '';
      FStatus       := usError;
      end;

    json.Free;
  end;

constructor TGitlabUpdater.Create(AProjectID: String);
  begin
    inherited Create;
    FServerRootURL := 'https://gitlab.com';
    FProjectID     := AProjectID;
  end;


{ TGithubUpdater }

function TGithubUpdater.GetLatestReleaseURL: String;
  begin
    Result := Format('%s/repos/%s/%s/releases/latest', [FServerRootURL, FUser, FProject]);
  end;

function TGithubUpdater.ExtractLatestVersion: Boolean;
  var
    json: TJSONData;
  begin
    Result := False;

      try
      json       := GetJSON(FPageLatest.DataString);
      FLatestVer := ParseVersion(json.GetPath('tag_name').AsString);
      Result     := True;
      except
      FStatus    := usError;
      end;

    json.Free;
  end;

function TGithubUpdater.ExtractMetaData: Boolean;
  var
    json, lnka, lnk: TJSONData;
    i:               Integer;
    url, size:       String;
  begin
    Result := False;

      try
      json := GetJSON(FPageLatest.DataString);
      lnka := json.GetPath('assets');

      if lnka.Count > 0 then
        for i := 0 to lnka.Count - 1 do
          begin
          lnk    := lnka.GetPath('[' + i.ToString + ']');
          url    := lnk.GetPath('browser_download_url').AsString;
          size   := lnk.GetPath('size').AsString;
          Result := Result or IsDownloadURLsGiven(url, size);
          IsWhatsNewURLsGiven(url);
          end;

      if not Result then FStatus := usNoFile;

      if Result and FWhatsNewNeed and (FWhatsNewName = '') then
        FWhatsNewData := json.GetPath('body').AsString;

      except
      FDownloadSize := 0;
      FDownloadURL  := '';
      FWhatsNewURL  := '';
      FStatus       := usError;
      end;

    json.Free;
  end;

constructor TGithubUpdater.Create(AUser: String; AProject: String);
  begin
    inherited Create;
    FServerRootURL := 'https://api.github.com';
    FUser          := AUser;
    FProject       := AProject;
  end;


{ TSourceForgeUpdater }

function TSourceForgeUpdater.GetLatestReleaseURL: String;
  begin
    Result := Format('%s/projects/%s/rss', [FServerRootURL, FProject]);
  end;

function TSourceForgeUpdater.ExtractLatestVersion: Boolean;
  var
    xml:  TXMLDocument;
    node: TDOMNode;
    str:  String;
  begin
    Result        := False;
    FLatestVerStr := '';
    FLatestVer    := Version(0, 0, 0, 0);
    FReleasesPath := FReleasesPath.Replace('(?:', '(').Replace('(', '(?:');
    //FPageLatest.LoadFromFile(Format('%s%s.txt', [FRootPath, FUpdateDir]));

      try
      FPageLatest.Position := 0;
      ReadXMLFile(xml, FPageLatest);

      node := xml.DocumentElement.FindNode('channel').FirstChild;
      repeat
        if node.NodeName = 'item' then
          begin
          str := LowerCase(String(node.FindNode('title').TextContent));

          if FReleasesPath <> '' then
            if FUseRegex then
              str := ReplaceRegExpr('/' + FReleasesPath + '(/.*)', str, '$1',
                [rroModifierI, rroUseSubstitution])
            else
              str := str.Remove(0, Length('/' + FReleasesPath));

          str := str.Split('/')[1];

          if NewerVersion(ParseVersion(str), FLatestVer) then
            begin
            FLatestVerStr := str;
            FLatestVer    := ParseVersion(str);
            Result        := True;
            end;
          end;
        node := node.NextSibling;
      until not Assigned(node);

      except
      FStatus := usError;
      Result  := False;
      end;

    xml.Free;
  end;

function TSourceForgeUpdater.ExtractMetaData: Boolean;
  var
    xml:  TXMLDocument;
    node: TDOMNode;
    str:  String;

  function Attr(AName: String): String;
    begin
      Result := node.FindNode('media:content').Attributes.GetNamedItem(AName).NodeValue;
    end;

  function GetUrl: String;
    begin
      Result := ReplaceRegExpr('(.*)(?:/download)', Attr('url'), '$1',
        [rroModifierI, rroUseSubstitution]);
    end;

  begin
    Result := False;

      try
      FPageLatest.Position := 0;
      ReadXMLFile(xml, FPageLatest);

      node := xml.DocumentElement.FindNode('channel').FirstChild;
      repeat
        if node.NodeName = 'item' then
          begin
          str := node.FindNode('title').TextContent;
          if String(ExtractFilePath(str)).Contains(FLatestVerStr) then
            if IsDownloadURLsGiven(str, '-') or IsWhatsNewURLsGiven(str) then
              begin
              Result := Result or IsDownloadURLsGiven(GetUrl, Attr('filesize'));
              IsWhatsNewURLsGiven(GetUrl);
              end;
          end;
        node := node.NextSibling;
      until not Assigned(node);

      if not Result then FStatus := usNoFile;

      except
      FDownloadSize := 0;
      FDownloadURL  := '';
      FWhatsNewURL  := '';
      FStatus       := usError;
      Result        := False;
      end;

    xml.Free;
  end;

constructor TSourceForgeUpdater.Create(AProject: String);
  begin
    inherited Create;
    FServerRootURL := 'https://sourceforge.net';
    FProject       := AProject;
  end;


end.
