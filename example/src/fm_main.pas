{ ou_test                                          |  (c) 2023 Riva   |  v1.0  |
  ------------------------------------------------------------------------------
  Demo app to show you how OnlineUpdaterPkg works.
  WARNINGS:
    1. This app can delete or replace some files! It's updater's job, you know. 
    2. It's strongly recommended to review code of this project before running it.
    3. Only run it if you know what you are doing!
  ------------------------------------------------------------------------------
  (c) Riva, 2023.10.28
  https://riva-lab.gitlab.io        https://gitlab.com/riva-lab
  ==============================================================================

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  -----------------------------------------------------------------------------}

unit fm_main;

{$mode objfpc}{$H+}

interface

uses
  SysUtils, Forms, Controls, StdCtrls, ExtCtrls, ComCtrls, Dialogs,
  Graphics, LCLIntf, LazFileUtils, Classes, TypInfo,
  OnlineUpdater, fm_updating;

type

  { TfmMain }

  TfmMain = class(TForm)
    mmLog:      TMemo;
    Panel1:     TPanel;
    btnCheck:   TButton;
    lbPos:      TLabel;
    pbBar:      TProgressBar;
    btnFull:    TButton;
    btnUpdate:  TButton;
    btnRestart: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnUpdateClick(Sender: TObject);
    procedure ouAvailable;
    procedure ouDownloadBegin;
    procedure ouDownloadDone;
    procedure ouUnzip;
    procedure ouError;
    procedure ouReplaceBegin;
    procedure ouReplaceDone;
    procedure ouDownloading(Sender: TObject; const ContentLength, CurrentPos: Int64);
    procedure ouUnzipping(Sender: TObject; const ATotPos, ATotSize: Int64);
    procedure btnCheckClick(Sender: TObject);
    procedure btnFullClick(Sender: TObject);
    procedure btnRestartClick(Sender: TObject);
    procedure Log(s: String);
    procedure Log(s: String; args: array of const);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private

  public

  end;

 {$DEFINE  GitLab } // comment to not test Gitlab
 {$DEFINE  GitHub } // comment to not test Github

const CParentDirPath = '..' + DirectorySeparator;

var
  fmMain:    TfmMain;
  timeStart: TDateTime;

  {$IFDEF GitLab}
  ou:        TGitlabUpdater;
  {$ELSE} {$IFDEF GitHub}
  ou:        TGithubUpdater;
  {$ELSE}
  ou:        TSourceForgeUpdater;
  {$ENDIF}
  {$ENDIF}


implementation

{$R *.lfm}

procedure TfmMain.Log(s: String);
  begin
    mmLog.Append(s);
  end;

procedure TfmMain.Log(s: String; args: array of const);
  begin
    Log(Format(s, args));
  end;

procedure TfmMain.FormCreate(Sender: TObject);
  begin
    mmLog.Clear;
    Log('FormCreate');
    Log('EXE: %s', [ParamStr(0)]);

  {$IFDEF GitLab}
    Caption := 'Online Updater Test - Gitlab.com';
    ou      := TGitlabUpdater.Create;

    { Gitlab.com specific requirement:
      to find your app on server provide ID of your project.
      !!! Warning! This app really exists! Run your test app with care! }
    ou.ProjectID := '29170126';

    { Name w/o extension of server-side zip file that contains files for update.
      Zip dirs and files structure must be the same as one of your project.
      Warning! Do not include extension .zip!
      !!! Warning! This app really exists! Run your test app with care! }
    //ou.ZipName := 'matrixfont-x64-portable';

    { You can also use regular expression for zip filename.
      This is useful when your zip filename contains version.
      Warning! Do not include extension .zip! }
    ou.ZipName := 'matrixfont-(x|win)64-portable';
  {$ELSE} {$IFDEF GitHub}
    Caption := 'Online Updater Test - Github.com';
    ou      := TGithubUpdater.Create;

    { !!! Warning! This app really exists! Run your test app with care!
      It will be better if you use your own project for testing under your full control!
      Warning! Do not include extension .zip! }
    ou.User    := 'AutoDarkMode';           // large zip >50M
    ou.Project := 'Windows-Auto-Night-Mode';
    ou.ZipName := 'AutoDarkModeX_(\d+\.)(\d+\.)(\d+\.)(\d+)';
  {$ELSE}
    Caption := 'Online Updater Test - SourceForge.net';
    ou      := TSourceForgeUpdater.Create;

    { SourceForge.net specific requirement:
      to find your app on server provide name of your project,
      and the path to directory that contains releases subdirs (optionally)
      !!! Warning! This app really exists! Run your test app with care! }
    ou.Project      := 'winscp';
    ou.ReleasesPath := 'winscp';
    ou.ZipName      := 'winscp-(\d+)(\.\d+)?(\.\d+)?(\.\d+)?(\.(beta|alpha|rc))?-Portable';
  {$ENDIF}
  {$ENDIF}

    { Provide event handlers to use all power of multithreaded implementation.
      Not all of them are necessary in your particular case. }
    ou.OnAvailable     := @ouAvailable;
    ou.OnDownloadBegin := @ouDownloadBegin;
    ou.OnDownloading   := @ouDownloading;
    ou.OnDownloadDone  := @ouDownloadDone;
    ou.OnUnzipping     := @ouUnzipping;
    ou.OnUnzip         := @ouUnzip;
    ou.OnError         := @ouError;
    ou.OnReplaceBegin  := @ouReplaceBegin;
    ou.OnReplaceDone   := @ouReplaceDone;

    { Path that contains whole your project which must be updated.
      Path must be relative to executable.
      Warning! All files in root and subdirs will be replaced during updating! }
    ou.RootPath := ExtractFilePath(ParamStr(0)) + CParentDirPath;
    Log('Root: %s', [CreateAbsolutePath(ou.RootPath, '')]);
    Log('Warning! All files in root and subdirs will be replaced during updating!');

    { Current actual version of your app.
      You can fill it using some libs/units that allows you read such info.
      But it is not nesessary, because updater can read version
      of this executable itself. Therefore, you must set checkbox
      "Include version info in executable" in project settings. }
    //ou.CurrentVersion := Version(0, 0, 0, 0);

    { We also can get some file with info what's new in update.
      It's usually a text file. But you can use any format.
      Please do not use large files, as this will result in slow checking for updates.
      Note that you must add an extension for this property.
      If WhatsNewNeed is True and you didn't specify WhatsNewName (i.e. '')
      then WhatsNewData will be filled with release description.
      Property WhatsNewNeed has False value by default. }
    ou.WhatsNewName := '.*?(readme|whats.?new).*?\.(txt|htm|html|md)';
    ou.WhatsNewNeed := True;

    { You can use regular expression to search filenames.
      This affects the ZipName and WhatsNewName properties.
      This is useful when your filenames contains modifiable data such as version.
      Default value is False. }
    ou.UseRegex := True;

    { Flag that allows updater run new app automatically after
      successfull old files replacing.
      By default it allows: value is True. }
    ou.RunAfterReplace := False;

    { If this property is set then old executable (with suffix .old)
      will be deleted after the new executable started.
      By default value is False. }
    //ou.OldExeDelete := True;

    { This flag is typically used to develop new updater classes,
      e.g. for a new server.  If BackupPageLatest = True,
      the txt file with the server response will be saved to disk. }
    //ou.BackupPageLatest := True;
  end;

procedure TfmMain.FormShow(Sender: TObject);
  begin
    if ou.StartReplacing then Hide;
    if ou.IsReplacing then Close;

    btnRestart.Enabled := False;
    Log('FormShow');

    with ou.CurrentVersion do
      Log('Current: v%d.%d.%d.%d', [Major, Minor, Revision, Build]);
  end;

procedure TfmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  begin
    if ou.IsDownloading then
      CanClose := MessageDlg('Warning',
        'Updater is downloading new files right now.' + LineEnding +
        'Do you really want to quit?',
        mtWarning, [mbOK, mbCancel], 0) = mrOk;
  end;

procedure TfmMain.btnCheckClick(Sender: TObject);
  begin
    Log('btnCheckClick');
    pbBar.Style := pbstMarquee;
    timeStart   := Now;
    ou.StartChecking;
  end;

procedure TfmMain.btnUpdateClick(Sender: TObject);
  begin
    Log('btnUpdateClick');
    ou.StartDownloading;
    if ou.IsDownloading then
      begin
      pbBar.Style    := pbstMarquee;
      pbBar.Position := 0;
      timeStart      := Now;
      end
    else
      Log('Please check for updates first.');
  end;

procedure TfmMain.btnFullClick(Sender: TObject);
  begin
    Log('btnFullClick');
    pbBar.Style    := pbstMarquee;
    pbBar.Position := 0;
    timeStart      := Now;
    ou.StartFullUpdating;
  end;

procedure TfmMain.btnRestartClick(Sender: TObject);
  begin
    // restart app to replace old files with new and finish updating
    if ou.IsReplacing then Close;
  end;

procedure TfmMain.ouAvailable;
  begin
    Log('Event: ouAvailable');
    Log('Working time: %s', [TimeToStr(Now - timeStart)]);
    pbBar.Style := pbstNormal;

    Log('Newer version of app is available.');
    with ou.LatestVer do
      Log('Latest:  v%d.%d.%d.%d', [Major, Minor, Revision, Build]);
    Log('URL: %s (%d bytes)', [ou.DownloadURL, ou.DownloadSize]);

    if ou.WhatsNewNeed and (ou.WhatsNewData <> '') then
      Log('WhatsNewData:' + LineEnding + ou.WhatsNewData);
  end;

procedure TfmMain.ouDownloadBegin;
  begin
    Log('Event: ouGetBegin');
    pbBar.Style    := pbstNormal;
    pbBar.Position := 0;
  end;

procedure TfmMain.ouDownloading(Sender: TObject; const ContentLength, CurrentPos: Int64);
  begin
    pbBar.Style := pbstNormal;
    if ContentLength - pbBar.Position < 100000 then Exit;

    pbBar.Max      := ContentLength;
    pbBar.Position := CurrentPos;
    lbPos.Caption  := Format('%d / %d KB', [CurrentPos div 1024, ContentLength div 1024]);
  end;

procedure TfmMain.ouDownloadDone;
  begin
    Log('Event: ouGetDone');
    Log('Working time: %s', [TimeToStr(Now - timeStart)]);
    pbBar.Style    := pbstNormal;
    pbBar.Position := 0;

    if ou.Status = usOK then
      Log('Unzipping...');
  end;

procedure TfmMain.ouUnzipping(Sender: TObject; const ATotPos, ATotSize: Int64);
  begin
    pbBar.Style := pbstNormal;
    if ATotPos - pbBar.Position < ATotSize div 100 then Exit;

    pbBar.Max      := ATotSize;
    pbBar.Position := ATotPos;
    lbPos.Caption  := Format('%d / %d KB', [ATotPos div 1024, ATotSize div 1024]);
  end;

procedure TfmMain.ouUnzip;
  begin
    Log('Event: ouUnzip');
    Log('Working time: %s', [TimeToStr(Now - timeStart)]);
    pbBar.Style        := pbstNormal;
    pbBar.Position     := pbBar.Max;
    btnRestart.Enabled := True;
    lbPos.Caption      := 'Unzip OK';

    if ou.Status = usOK then
      Log('Ready to update. Restart app to complete.');
  end;

procedure TfmMain.ouReplaceBegin;
  begin
    Log('Event: ouReplaceBegin');
    Log('Online Updater is updating your app now');
    Log('Copying new files...');

    { Show special little window to indicate updating progress,
      because user prefer to see working app instead of nothing. :)
      If your app isn't large and the copying process is fast,
      you don't have to use this event handler. }
    with fmUpdating do
      begin
      Label1.Caption := 'Online Updater is updating your app now';
      FormStyle      := fsSystemStayOnTop;
      Show;
      end;
  end;

procedure TfmMain.ouReplaceDone;
  begin
    Log('Event: ouReplaceDone');

    // updating is finished:
    fmUpdating.Close;

    { If we allow run new app automatically then close old app,
      new app has already started.
      Otherwise show this app form which was previously hidden. }
    if ou.RunAfterReplace then Close else Show;

    // if we don't allow run new app automatically then show this message
    if not ou.RunAfterReplace then Log(
        'Congrats! Your app was successfully updated.' + LineEnding +
        'Close this old app and start use new one.' + LineEnding +
        'It is recommended to set property RunAfterReplace := True (by default) in real case.');
  end;

procedure TfmMain.ouError;
  var
    s: String = '';
  begin
    Log('Event: ouError');
    Log('Working time: %s', [TimeToStr(Now - timeStart)]);
    pbBar.Style := pbstNormal;

    case ou.Status of
      usWorking: s     := 'Updater is working now. Please wait a moment.';
      usUnchecked: s   := 'We must now what exactly we need to download. Check for updates first.';
      usOffline: s     := 'It seems you are offline. Check your network.';
      usNoUpdates: s   := 'No new version. Be patient and wait till author releases one.';
      usInvalidLink: s := 'Incorrect URL to get updates. Check project settings.';
      usNoFile: s      := 'No such file on server. Check ZIP name (without extension).';
      usDLError: s     := 'Downloading error. Something went wrong.';
      usZipError: s    := 'ZIP file may be wrong. Check it.';
      usNotReplaced: s := 'Replacing files error.';
      usError: s       := 'Other error. Something went wrong.';
      end;

    Log('Status = %s:', [GetEnumName(TypeInfo(TUpdaterStatus), Ord(ou.Status))]);
    Log('  ' + s);
  end;

end.
